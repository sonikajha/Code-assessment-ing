package com.ing.mortgageservices.service;


import com.ing.mortgageservices.dao.MortgageServicesDAO;

import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageOutputDTO;
import com.ing.mortgageservices.model.MortgageRate;

import org.junit.Test;
import org.junit.Before;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

import org.mockito.InjectMocks;
import org.mockito.Mock;


import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
public class MortgageServiceTest {

    @InjectMocks
    private MortgageService mortgageService;

    @Mock
    private MortgageServicesDAO mortgageServicesDAO;

    private static List<MortgageRate> mortgageRateList = new ArrayList<>();

   @Before
    public void setup(){
        initMocks(this);

       MortgageRate mortgageRate = new MortgageRate();
       mortgageRate.setIntRate(8.0);
       mortgageRate.setMaturityPeriod(10);
       mortgageRateList.add(mortgageRate);
    }

    @Test
    public void shouldRetrieveInterestRates(){
        //given - list of mortgage rates - mortgageRateList

        //when
        when(mortgageServicesDAO.retrieveAllRates()).thenReturn(mortgageRateList);
        List<Double> actual = mortgageService.retrieveInterestRates();

        //then
        assertEquals(8.0, actual.get(0), 0.0);
        verify(mortgageServicesDAO).retrieveAllRates();
    }

    @Test
    public void shouldCheckMortgage(){
        //given
        MortgageInputDTO mortgageInputDTO = new MortgageInputDTO();
        mortgageInputDTO.setHomeValue(20);
        mortgageInputDTO.setLoanValue(18);
        mortgageInputDTO.setMaturityPeriod(10);
        mortgageInputDTO.setIncome(200);

        //when
        when(mortgageServicesDAO.retrieveAllRates()).thenReturn(mortgageRateList);
        when(mortgageServicesDAO.checkMortgage(mortgageInputDTO)).thenReturn(mortgageInputDTO);
        MortgageOutputDTO actual = mortgageService.checkMortgage(mortgageInputDTO);

        //then
        assertTrue(actual.isFeasible());
        assertEquals(actual.getMonthlyAmount(),0.225,0.50);
    }

}
