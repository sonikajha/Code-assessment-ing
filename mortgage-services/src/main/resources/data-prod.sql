insert into mortgage_Rate (id, maturity_period,int_rate,last_upd_date) values (1,5,4.5,current_timestamp());
insert into mortgage_Rate (id, maturity_period,int_rate,last_upd_date) values (2,10,5,current_timestamp());
insert into mortgage_Rate (id, maturity_period,int_rate,last_upd_date) values (3,15,5.5,current_timestamp());
insert into mortgage_Rate (id, maturity_period,int_rate,last_upd_date) values (4,20,6,current_timestamp());
insert into mortgage_Rate (id, maturity_period,int_rate,last_upd_date) values (5,25,6.5,current_timestamp());