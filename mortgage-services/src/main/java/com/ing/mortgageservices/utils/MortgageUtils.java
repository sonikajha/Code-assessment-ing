package com.ing.mortgageservices.utils;

import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageRate;

import java.math.BigDecimal;
import java.util.List;

public class MortgageUtils {

    public static boolean isMortgageFeasible(MortgageInputDTO mortgageInputDTO){

        if (mortgageInputDTO.getHomeValue() > mortgageInputDTO.getLoanValue()
                && mortgageInputDTO.getIncome()>= Math.multiplyExact(4, ((long) mortgageInputDTO.getLoanValue()))){
            return true;
        }
        return false;
    }

    public static double calculateMonthlyAmount(MortgageInputDTO mortgageInputDTO, List<MortgageRate> mortgageRate){
        // logic to calculate interest and the monthly amount, would be ideally a complex business logic that is simplified here
        for (MortgageRate mortgage: mortgageRate
        ) {
            if (mortgageInputDTO.getMaturityPeriod() == mortgage.getMaturityPeriod()){
                double simpleInterest = mortgageInputDTO.getLoanValue()* mortgage.getIntRate()* mortgageInputDTO.getMaturityPeriod()/100;
                return mortgageInputDTO.getLoanValue() + simpleInterest/12*mortgageInputDTO.getMaturityPeriod();
            }
        }
        return 0;
    }
}
