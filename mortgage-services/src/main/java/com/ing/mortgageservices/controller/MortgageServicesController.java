package com.ing.mortgageservices.controller;

import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageOutputDTO;
import com.ing.mortgageservices.service.MortgageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/***
 * @author Sonika
 * @version 1.1
 * @apiNote REST APIs Controller for GET interest rates and POST mortgage checks
 */
@RestController
public class MortgageServicesController {

    @Autowired
    private MortgageService mortgageService;

    @GetMapping("/api/interest-rates")
    public List<Double> retrieveInterestRates() {
        return mortgageService.retrieveInterestRates();
    }

    @PostMapping("/api/mortgages")
    public ResponseEntity<MortgageOutputDTO> checkMortgage(
            @Valid @RequestBody MortgageInputDTO newMortgageRequest) {

        MortgageOutputDTO mortgageOutputDTO = mortgageService.checkMortgage(newMortgageRequest);

        if (mortgageOutputDTO == null)
            return ResponseEntity.badRequest().build();

        return ResponseEntity.ok().body(mortgageOutputDTO);
    }
}
