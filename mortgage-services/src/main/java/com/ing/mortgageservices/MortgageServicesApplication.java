package com.ing.mortgageservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/***
 * @author Sonika
 * @implNote Entry point into the mortgage services
 */
@SpringBootApplication
public class MortgageServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MortgageServicesApplication.class, args);
	}
}
