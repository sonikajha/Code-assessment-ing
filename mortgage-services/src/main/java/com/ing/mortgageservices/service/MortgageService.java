package com.ing.mortgageservices.service;

import com.ing.mortgageservices.dao.MortgageServicesDAO;
import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageOutputDTO;
import com.ing.mortgageservices.model.MortgageRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/***
 * @author Sonika
 * @implNote Service class to handle incoming requests and providing expected response
 */
@Component
public class MortgageService {

    @Autowired
    private MortgageServicesDAO mortgageServicesDAO;

    /***
     * Retrieves all interest rates populated into the mortgage rate table
     * @return
     */
    public List<Double> retrieveInterestRates() {

        List<MortgageRate> mortgageRate = mortgageServicesDAO.retrieveAllRates();
        // use streams
        List<Double> interestRates = new ArrayList();

        for (MortgageRate rate : mortgageRate) {
            interestRates.add(rate.getIntRate());
        }

        return interestRates;

    }

    /***
     * Services the POST api for mortgage check
     * @param mortgageInputDTO
     * @return mortgageOutputDTO
     */
    public MortgageOutputDTO checkMortgage(MortgageInputDTO mortgageInputDTO) {
        boolean isFeasible = isMortgageFeasible(mortgageInputDTO);
        MortgageOutputDTO mortgageOutputDTO = new MortgageOutputDTO();
        if (isFeasible) {
            mortgageServicesDAO.checkMortgage(mortgageInputDTO);
            mortgageOutputDTO.setFeasible(true);
            mortgageOutputDTO.setMonthlyAmount(calculateMonthlyAmount(mortgageInputDTO, mortgageServicesDAO.retrieveAllRates()));
        } else {
            mortgageOutputDTO.setFeasible(false);
        }

        return mortgageOutputDTO;
    }

    /***
     * Util method to calculate if mortgage is feasible
     * @param mortgageInputDTO
     * @return
     */
    private boolean isMortgageFeasible(MortgageInputDTO mortgageInputDTO) {

        if (mortgageInputDTO.getHomeValue() > mortgageInputDTO.getLoanValue()
                && mortgageInputDTO.getIncome() >= Math.multiplyExact(4, ((long) mortgageInputDTO.getLoanValue()))) {
            return true;
        }
        return false;
    }

    /***
     * Util method to calculate the monthly mortgage amount
     * @param mortgageInputDTO
     * @param mortgageRate
     * @return
     */
    private double calculateMonthlyAmount(MortgageInputDTO mortgageInputDTO, List<MortgageRate> mortgageRate) {
        // logic to calculate interest and the monthly amount, would be ideally a complex business logic that is simplified here
        for (MortgageRate mortgage : mortgageRate
        ) {
            if (mortgageInputDTO.getMaturityPeriod() == mortgage.getMaturityPeriod()) {
                double simpleInterest = mortgageInputDTO.getLoanValue() * mortgage.getIntRate() * mortgageInputDTO.getMaturityPeriod() / 100;
                return (mortgageInputDTO.getLoanValue() + simpleInterest) / (12 * mortgageInputDTO.getMaturityPeriod());
            }
        }
        return 0;
    }
}
