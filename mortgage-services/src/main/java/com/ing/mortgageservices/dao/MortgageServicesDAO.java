package com.ing.mortgageservices.dao;

import com.ing.mortgageservices.jpa.MortgageRatesRepository;
import com.ing.mortgageservices.jpa.MortgageRepository;
import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/***
 * @author Sonika
 * @version 1.1
 * @apiNote Data Access Object for Mortgage Services to fetch and persist to repository
 */

@Component
public class MortgageServicesDAO {

    @Autowired
    private MortgageRatesRepository mortgageRatesRepository;

    @Autowired
    private MortgageRepository mortgageRepository;

    /***
     * Gets all mortgage rates from MortgageRateRepository
     * @return List<MortgageRate>
     */
    public List<MortgageRate> retrieveAllRates() {
        return mortgageRatesRepository.findAll();
    }

    /***
     * Persists the POST request for mortgage check
     * @param mortgageInputDTO
     * @return MortgageInputDTO
     */
    public MortgageInputDTO checkMortgage(MortgageInputDTO mortgageInputDTO) {
        return mortgageRepository.save(mortgageInputDTO);
    }



}
