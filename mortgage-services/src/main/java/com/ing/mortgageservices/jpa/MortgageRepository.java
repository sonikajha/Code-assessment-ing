package com.ing.mortgageservices.jpa;

import com.ing.mortgageservices.model.MortgageInputDTO;
import com.ing.mortgageservices.model.MortgageOutputDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/***
 * @author Sonika
 * @implNote JPA repository for Mortgage checks
 */

@Repository
public interface MortgageRepository extends JpaRepository<MortgageInputDTO, Long>{

}