package com.ing.mortgageservices.jpa;

import com.ing.mortgageservices.model.MortgageRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/***
 * @author Sonika
 * @implNote JPA repository for Mortgage rates
 */
@Repository
public interface MortgageRatesRepository extends JpaRepository<MortgageRate, Long>{

}