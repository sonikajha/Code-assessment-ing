package com.ing.mortgageservices.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
/***
 * @author Sonika
 * @implNote SecurityConfig for the API for dev region
 */
@Configuration
@Profile("dev")
public class MortgageServicesSecurityConfig extends WebSecurityConfigurerAdapter {
    /***
     * Currently allows public access to the APIs
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/mortgages").permitAll()
                .anyRequest().authenticated()
                .and()
                .antMatcher("actuator/**").authorizeRequests();
    }
}
