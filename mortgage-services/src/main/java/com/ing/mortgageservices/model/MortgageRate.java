package com.ing.mortgageservices.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.sql.Timestamp;
/***
 * @author Sonika
 * @implNote Entity for mortgage rates
 */
@Entity
public class MortgageRate {

    @Id
    @GeneratedValue
    private long id;
    private int maturityPeriod;
    private double intRate;
    private Timestamp lastUpdDate;

    public MortgageRate(){
        super();
    }

    public MortgageRate(int maturityPeriod, double intRate, Timestamp lastUpdDate){
        this.maturityPeriod=maturityPeriod;
        this.intRate=intRate;
        this.lastUpdDate=lastUpdDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMaturityPeriod() {
        return maturityPeriod;
    }

    public void setMaturityPeriod(int maturityPeriod) {
        this.maturityPeriod = maturityPeriod;
    }

    public double getIntRate() {
        return intRate;
    }

    public void setIntRate(double intRate) {
        this.intRate = intRate;
    }

    public Timestamp getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Timestamp lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }
}
