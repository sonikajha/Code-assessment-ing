package com.ing.mortgageservices.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Positive;
import java.util.Objects;

/***
 * @author Sonika
 * @implNote Entity/model/DTO for mortgage check POST api, also handles input validation using hibernate validator
 */

@Entity
public class MortgageInputDTO {

    @Id
    @GeneratedValue
    private int id;
    @Positive(message = "Please provide a valid input for income.")
    private double income;

    @Positive(message = "Please provide a valid input for home value.")
    private double homeValue;
    @Positive(message = "Please provide a valid input for loan value.")
    private double loanValue;
    @Positive(message = "Please provide a valid non-zero input for maturity period.")
    private int maturityPeriod;

    public MortgageInputDTO() {

    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getHomeValue() {
        return homeValue;
    }

    public void setHomeValue(double homeValue) {
        this.homeValue = homeValue;
    }

    public double getLoanValue() {
        return loanValue;
    }

    public void setLoanValue(double loanValue) {
        this.loanValue = loanValue;
    }

    public int getMaturityPeriod() {
        return maturityPeriod;
    }

    public void setMaturityPeriod(int maturityPeriod) {
        this.maturityPeriod = maturityPeriod;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MortgageInputDTO that = (MortgageInputDTO) o;
        return Double.compare(that.income, income) == 0 &&
                Double.compare(that.homeValue, homeValue) == 0 &&
                Double.compare(that.loanValue, loanValue) == 0 &&
                Objects.equals(maturityPeriod, that.maturityPeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(income, homeValue, loanValue, maturityPeriod);
    }

    @Override
    public String toString() {
        return "MortgageInputDTO{" +
                "income=" + income +
                ", homeValue=" + homeValue +
                ", loanValue=" + loanValue +
                ", maturityPeriod=" + maturityPeriod +
                '}';
    }
}