package com.ing.mortgageservices.model;

import org.springframework.stereotype.Component;

import java.util.Objects;

/***
 * @author Sonika
 * @implNote DTO for mortgage check POST api response
 */

/*@Component*/
public class MortgageOutputDTO {

    boolean isFeasible;
    double monthlyAmount;

    public boolean isFeasible() {
        return isFeasible;
    }

    public void setFeasible(boolean feasible) {
        isFeasible = feasible;
    }

    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MortgageOutputDTO that = (MortgageOutputDTO) o;
        return isFeasible == that.isFeasible &&
                Double.compare(that.monthlyAmount, monthlyAmount) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isFeasible, monthlyAmount);
    }

    @Override
    public String toString() {
        return "MortgageOutputDTO{" +
                "isFeasible=" + isFeasible +
                ", monthlyAmount=" + monthlyAmount +
                '}';
    }
}
